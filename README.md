# membership management

This is a collection of scripts for management of the mailing list and printing
of membership cards.

## membership-cards

The scripts in `membership-cards\` are for producing a PDF which can then be
printed on our card printer.
See `demo-list.csv` for format. Input is taken from `list.csv`.
Dependencies:
* node dependencies (install with `npm i` while in `membership-cards\`)
* Inkscape https://inkscape.org/ (version >=1.0)
* The font Bebas Neue (https://www.fontfabric.com/fonts/bebas-neue/)

## mailing-list [OBSOLETE?]

The script in `mailing-list/` simply takes a list of crsIDs and/or email
addresses (as e.g. pasted from a sign-up spreadsheet) and produces a file
whose contents can be pasted into Mailman.
