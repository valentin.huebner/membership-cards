const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const csv = require('csvtojson');
const confirm = require('inquirer-confirm');
const createMultiPDF = require('./../create-pdf');

const DEFAULT_DATE = '–';
const INPUT_FILE = 'list.csv';

(async () => {
  const templates = {
    trinity: await readFile('template-trinity.svg', 'utf8'),
    nonTrinity: await readFile('template-non-trinity.svg', 'utf8')
  }

  var members = await csv().fromFile(INPUT_FILE);
  for (let member of members) {
    if (!member.crsID || member.crsID.includes('@')) {
      member.crsID = '–';
    }
    member.college = member.college.replace('’', '\'') || '–';
    member.date = member.date || DEFAULT_DATE;
    if (member.name.length > 18) {
      let names = member.name.split(' ');
      let lastName = names.pop();
      let initials = names.map(n => n.charAt(0));
      member.name = `${initials.join(' ')} ${lastName}`;
      console.log(`Shortened name: ${member.name}`);
    }
  }

  var colleges = new Set(members.map(m => m.college));
  console.log('List of colleges:');
  console.log(Array.from(colleges).sort());
  try {
    await confirm('Does this look alright?');
  } catch {
    console.error('Abort.');
    process.exit(1);
  }

  var cards = members.map(member => ({
    member: member,
    template: member.college == 'Trinity'
        ? templates.trinity : templates.nonTrinity
  }));
  await createMultiPDF(cards);
})();
