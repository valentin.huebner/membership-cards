const os = require('os');
const process = require('process');
const randomstring = require('randomstring');
const pathLib = require('path');
const fs = require('fs');
const childProcess = require('child_process');
const util = require('util');
const exec = util.promisify(childProcess.exec);
const writeFile = util.promisify(fs.writeFile);

const TMP_DIR = os.tmpdir();
const OUTPUT_FILE = 'cards.pdf';

async function createPDF (member, template, path) {
  var svgPath = `${path}.svg`;
  var pdfPath = `${path}.pdf`;

  var svg = template
      .replace('%%ARCHIM-NAME%%', member.name)
      .replace('%%ARCHIM-COLLEGE%%', member.college)
      .replace('%%ARCHIM-DATE%%', member.date)
      .replace('%%ARCHIM-MEMBERSHIP%%', member.membership);

  await writeFile(svgPath, svg, 'utf8');
  await exec(`inkscape ${svgPath} -A ${pdfPath}`);

  console.log(`Created card for ${member.crsID}`);
  return pdfPath;
}

async function createMultiPDF (cards) {
  console.log('Creating cards ...');
  var promises = cards.map(({ member, template }) => {
    var path = pathLib.join(TMP_DIR, randomstring.generate(10));
    return createPDF(member, template, path);
  });
  var PDFpaths = await Promise.all(promises);

  console.log('Combining PDFs ...');
  await exec(`pdfunite ${PDFpaths.join(' ')} ${OUTPUT_FILE}`);
  console.log(`Output at ${OUTPUT_FILE}`);
}

module.exports = createMultiPDF;
