const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const csv = require('csvtojson');
const confirm = require('inquirer-confirm');
const createMultiPDF = require('./../create-pdf');

const DEFAULT_DATE = 'October 2018';
const DEFAULT_EXPIRY = 'October 2019';
const INPUT_FILE = 'list.csv';

(async () => {
  const template = await readFile('template.svg', 'utf8');

  var members = await csv().fromFile(INPUT_FILE);
  for (let member of members) {
    if (!member.crsID || member.crsID.includes('@')) {
      member.crsID = '–';
    }
    member.college = member.college.replace('’', '\'') || '–';
    if (member.college != '–' && !(member.college.endsWith('College')
        || member.college.endsWith('Hall'))) {
      member.college += ' College';
    }
    member.date = DEFAULT_DATE;
    member.name = `${member.firstName} ${member.lastName}`;
    member.expiry = DEFAULT_EXPIRY;
    member.membership = member.type == 'Life'
        ? 'Life membership' : 'One-year membership until ' + member.expiry;

    // if (member.name.length > 18) {
    //   let names = member.name.split(' ');
    //   let lastName = names.pop();
    //   let initials = names.map(n => n.charAt(0));
    //   member.name = `${initials.join(' ')} ${lastName}`;
    //   console.log(member.name);
    // }
  }

  var colleges = new Set(members.map(m => m.college));
  console.log('List of colleges:');
  console.log(Array.from(colleges).sort());
  try {
    await confirm('Does this look alright?');
  } catch {
    console.error('Abort.');
    process.exit(1);
  }

  var cards = members.map(member => ({
    member: member,
    template: template
  }));
  await createMultiPDF(cards);
})();
