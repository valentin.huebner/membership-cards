const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

var INPUT_FILE = 'input';
var OUTPUT_FILE = 'output';

(async () => {
  var input = await readFile(INPUT_FILE, 'utf8');
  var lines = input.split('\n');
  lines = lines.filter(l => l);
  var emailAddresses = lines.map(line => {
    if (line.includes('@')) return line;
    else return `${line}@cam.ac.uk`;
  });
  var output = emailAddresses.join('\n') + '\n';
  await writeFile(OUTPUT_FILE, output, 'utf8');
  console.log(`Output written to \`${OUTPUT_FILE}\`.`);
})();
